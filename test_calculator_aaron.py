import pytest
from calculator import calculator


@pytest.mark.parametrize("x, y, operation, result", [
    (5, 3, "+", 8),
    (9, 4, "-", 5),
    (7, 3, "*", 21),
    (8, 2, "/", 4),
])
def test_calculator_basic_operations(x, y, operation, result):
    assert calculator(x, y, operation) == result


def test_calculator_invalid_operator():
    assert calculator(5, 3, "%") == "Invalid operator"


def test_calculator_wrong_input_number():
    assert calculator(-1, 4, "+") == "Wrong Input number"
    assert calculator(11, 4, "+") == "Wrong Input number"
    assert calculator(5, -1, "+") == "Wrong Input number"
    assert calculator(5, 11, "+") == "Wrong Input number"


@pytest.mark.parametrize("operation", ["+", "-", "*", "/"])
def test_calculator_valid_input_range(operation):
    for i in range(11):
        for j in range(11):
            assert calculator(i, j, operation) is not None
