def calculator(a, b, operator):
    if a < 1 or a > 10 or b < 1 or b > 10:
        return "Wrong Input number"

    if operator == "+":
        return a + b
    elif operator == "-":
        return a - b
    elif operator == "*":
        return a * b
    elif operator == "/":
        return a / b
    else:
        return "Invalid operator"


def main():
    a = int(input("Enter number 1-10: "))
    b = int(input("Enter number 1-10: "))
    operator = input("Enter operator: ")
    result = calculator(a, b, operator)
    print(result)


if __name__ == "__main__":
    main()